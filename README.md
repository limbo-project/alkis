# ALKIS Ontology
The ontologies housed in this repository have been developed as a geo data Onotlogy in order to link ALKIS data to the Semantic Web.

[Diagrams](diagrams)

[Documents](docs)

[Data](hamburg)

# ALKIS Class diagram
![Class diagram](diagrams/ALKIS.Diagram.png "ALKIS class diagram")

# Machine-interpretable version
[alkis.owl](https://github.com/vocol/alkis/blob/master/alkis.owl)
